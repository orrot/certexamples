package util;

import exceptions.UncheckedDivisionByZeroException;

import java.io.IOException;

public class AutocloseableResource implements AutoCloseable {
    
    public void open() {
        System.out.println("Open: AutocloseableResource");
    }
    
    public void launchInvalidOperation() {
        throw new UncheckedDivisionByZeroException("Unchecked Exception");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Close: AutocloseableResource");
        throw new IOException("Internal error in the AutocloseableResource");
    }
}
