package collectors.examples.collectors.examples.types;

public class Estudiante {
    
    public enum Grado {

        NOVENO, DECIMO, ONCE;
    }
    
    private String nombre;
    private int edad;
    private Grado grado;

    public Estudiante(String nombre, int edad, Grado colegio) {
        this.nombre = nombre;
        this.edad = edad;
        this.grado = colegio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }
    
    public String toString() {
        return new StringBuilder().append("{")
                .append(nombre).append(", ")
                .append(edad).append(", ")
                .append(grado)
                .append("}")
                .toString();
        
    }
}
