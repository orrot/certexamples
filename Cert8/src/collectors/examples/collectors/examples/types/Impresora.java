package collectors.examples.collectors.examples.types;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Impresora {
    
    public static void imprimir(String titulo, Collection<?> lista) {

        System.out.println("\n");
        System.out.println("================ " + titulo + "================ ");
        lista.forEach(System.out::println);
    }

    public static void imprimir(String titulo, Map<?, ?> mapa) {

        System.out.println("\n");
        System.out.println("================ " + titulo + "================ ");
        mapa.forEach((k, v) -> System.out.println(k + "::" + v));
    }

    public static void imprimir(String titulo, Object objeto) {

        System.out.println("\n");
        System.out.println("****** " + titulo + " :: " + objeto);
    }

    public static void imprimir(Object objeto) {

        System.out.println("\n");
        System.out.println(objeto);
    }
}
