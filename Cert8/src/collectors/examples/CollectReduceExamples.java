package collectors.examples;

import collectors.examples.collectors.examples.types.Estudiante;
import collectors.examples.collectors.examples.types.Impresora;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CollectReduceExamples {
    
    public static List<Estudiante> generarEstudiantes() {
        
        List<Estudiante> estudiantes = new ArrayList<>(20);
        estudiantes.add(new Estudiante("Manuel Felipe Diaz", 15, Estudiante.Grado.NOVENO));
        estudiantes.add(new Estudiante("Luis Trujillo", 18, Estudiante.Grado.ONCE));
        estudiantes.add(new Estudiante("Naruto Usumaki", 16, Estudiante.Grado.DECIMO));
        estudiantes.add(new Estudiante("Sasuke Uchija", 14, Estudiante.Grado.DECIMO));
        estudiantes.add(new Estudiante("Kakaroto de Jesus", 15, Estudiante.Grado.DECIMO));
        estudiantes.add(new Estudiante("Mikasa Acherman", 13, Estudiante.Grado.NOVENO));
        estudiantes.add(new Estudiante("Levi Acherman", 19, Estudiante.Grado.ONCE));
        estudiantes.add(new Estudiante("Eren Jaeger ", 13, Estudiante.Grado.NOVENO));
        estudiantes.add(new Estudiante("Lelouch Lamperouge", 14, Estudiante.Grado.NOVENO));
        estudiantes.add(new Estudiante("Mike Wheeler", 12, Estudiante.Grado.NOVENO));
        estudiantes.add(new Estudiante("Nancy Wheeler", 19, Estudiante.Grado.ONCE));
        return estudiantes;
    }
    
    public static void main(String[] args) {

        List<Estudiante> estudiantes = generarEstudiantes();

        // Ejercicio 1
        // Hallar los estudiantes mayores de edad
        ejercicio1(estudiantes);
        
        // Ejercicio 2 
        // Hallar el promedio de edad de todos los estudiantes
        ejercicio2(estudiantes);

        // Ejercicio 3
        // Obtener el listado de estudiantes de Once
        ejercicio3(estudiantes);

        // Ejercicio 4
        // Ordenar los estudiantes de noveno por edad y por nombre
        ejercicio4(estudiantes);

        // Ejercicio 5
        // Obtener el listado de nombres de todos los estudiantes de once ordenados alfabeticamente
        ejercicio5(estudiantes);

        // Ejercicio 6
        // Agrupar los estudiantes de acuerdo a su grado
        ejercicio6(estudiantes);

        // Ejercicio 7
        // Agrupar los nombres de los estudiantes de acuerdo a su grado
        ejercicio7(estudiantes);

        // Ejercicio 8
        // Obtener el listado de estudiantes divididos entre mayores y menores de edad
        ejercicio8(estudiantes);

        // Ejercicio 9
        // Hallar el promedio de edad de todos los estudiantes por grado
        ejercicio9(estudiantes);

    }
    
    public static void ejercicio1(List<Estudiante> estudiantes) {
        
        List<Estudiante> estudiantesMayores = estudiantes
                .stream()
                .filter(e -> e.getEdad() >= 18)
                .collect(Collectors.toList());
        Impresora.imprimir("=========== EJERCICIO 1 ==============", estudiantesMayores);
    }

    public static void ejercicio2(List<Estudiante> estudiantes) {
        double avg = estudiantes
                .stream()
                .mapToInt(Estudiante::getEdad)
                .average()
                .getAsDouble();
        Impresora.imprimir("=========== EJERCICIO 2 ==============", avg);
    }

    public static void ejercicio3(List<Estudiante> estudiantes) {
        
        
        List<Estudiante> estudiantesOnce = estudiantes
                .stream()
                .filter(es -> Estudiante.Grado.ONCE.equals(es.getGrado()))
                .collect(Collectors.toList());
        
        Impresora.imprimir("=========== EJERCICIO 3 ==============",estudiantesOnce );
    }

    public static void ejercicio4(List<Estudiante> estudiantes) {
        List<Estudiante> estudiantesOrdenados = estudiantes
                .stream()
                .filter(es-> Estudiante.Grado.NOVENO.equals(es.getGrado()))
                .sorted(Comparator.comparing(Estudiante::getEdad).thenComparing(Estudiante::getNombre).reversed())
                .collect(Collectors.toList());
                
        Impresora.imprimir("=========== EJERCICIO 4 ==============", estudiantesOrdenados);
    }

    public static void ejercicio5(List<Estudiante> estudiantes) {
        List<String> estudiantesOrdenados = estudiantes
                .stream()
                .filter(estudiante -> Estudiante.Grado.ONCE.equals(estudiante.getGrado()))
                .map(Estudiante::getNombre)
                .sorted()
                .collect(Collectors.toList());
        Impresora.imprimir("=========== EJERCICIO 5 ==============", estudiantesOrdenados);
    }

    public static void ejercicio6(List<Estudiante> estudiantes) {
        Map<Estudiante.Grado, List<Estudiante>> estudianteMap = estudiantes
                .stream()
                .collect(Collectors.groupingBy(Estudiante::getGrado));
        Impresora.imprimir("=========== EJERCICIO 6 ==============", estudianteMap);
    }

    public static void ejercicio7(List<Estudiante> estudiantes) {
        Map<Estudiante.Grado, List<String>> list =  estudiantes
                .stream()
                .collect(Collectors.groupingBy(Estudiante::getGrado, Collectors.mapping(Estudiante::getNombre, Collectors.toList())));
        Impresora.imprimir("=========== EJERCICIO 7 ==============", list);
    }

    public static void ejercicio8(List<Estudiante> estudiantes) {
        Map<Boolean, List<Estudiante>> list =  estudiantes
                .stream()
                .collect(Collectors.partitioningBy((Estudiante es) -> es.getEdad()<18));
        Impresora.imprimir("=========== EJERCICIO 8 ==============", list);
    }

    public static void ejercicio9(List<Estudiante> estudiantes) {
        Map<Estudiante.Grado, Double> list =  estudiantes
                .stream()
                .collect(Collectors.groupingBy(Estudiante::getGrado, Collectors.averagingDouble(Estudiante::getEdad)));
        Impresora.imprimir("=========== EJERCICIO 9 ==============", list);
    }
}
