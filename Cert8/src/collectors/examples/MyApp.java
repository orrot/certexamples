package collectors.examples;

import java.util.Arrays;
import java.util.List;

public class MyApp {

    public static void main(String[] args) {

        List<Book> books = Arrays.asList(

                new Book("Atlas Shrugged", Book.Category.FICTION),

                new Book("A Brief History of Time", Book.Category.NON_FICTION)

        );

        books.stream().filter(b ->b.getCategory().equals(Book.Category.FICTION))

                .peek(b ->System.out.println(b.getTitle()));

    }

}



class Book {

    private String title;

    private Category category;

    Book(String name, Category category) {

        this.title = name;

        this.category = category;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    static enum Category {

        FICTION, NON_FICTION

    };

}