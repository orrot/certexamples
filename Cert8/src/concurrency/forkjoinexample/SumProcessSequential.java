package concurrency.forkjoinexample;

import java.util.List;
import java.util.stream.Stream;

public class SumProcessSequential {

    public static Integer sumNumbers(List<Integer> numbers) {

        return numbers.stream()
                .reduce(0, SlowProcessesUtils::sumSlow);
    }
}
