package concurrency.forkjoinexample.design;

import concurrency.forkjoinexample.SlowProcessesUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

public class SumProcessSplittingBy2Task extends RecursiveTask<Integer> {

    private List<Integer> numbers;

    public SumProcessSplittingBy2Task(List<Integer> numbers) {
        this.numbers = new ArrayList<>(numbers);
    }

    @Override
    protected Integer compute() {

        if (numbers.size() <= 2) {

            switch (numbers.size()) {
                case 0:
                    return 0;
                case 1:
                    return SlowProcessesUtils.sumSlow(0, numbers.get(0));
                case 2:
                    return SlowProcessesUtils.sumSlow(numbers.get(0), numbers.get(1));
            }
        }

        List<ForkJoinTask<Integer>> tasks = new ArrayList<>();
        for (int i = 0; i < numbers.size(); i+=2) {

            List<Integer> twoNumbers;
            if (i + 2 > numbers.size()) {
                twoNumbers = numbers.subList(i, i + 1);
            } else {
                twoNumbers = numbers.subList(i, i + 2);
            }

            SumProcessSplittingBy2Task sumProcessTask = new SumProcessSplittingBy2Task(twoNumbers);
            ForkJoinTask<Integer> forkTask = sumProcessTask.fork();
            tasks.add(forkTask);
        }

        List<Integer> sumedNumbers = tasks.stream()
                .map(ForkJoinTask::join)
                .collect(Collectors.toList());

        SumProcessSplittingBy2Task sumProcessTaskTotals = new SumProcessSplittingBy2Task(sumedNumbers);
        return sumProcessTaskTotals.compute();
    }
}
