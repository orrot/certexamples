package concurrency.forkjoinexample.design;

import concurrency.forkjoinexample.SlowProcessesUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

public class SumProcessRecursiveTask extends RecursiveTask<Integer> {

    private List<Integer> numbers;

    public SumProcessRecursiveTask(List<Integer> numbers) {
        this.numbers = new ArrayList<>(numbers);
    }

    @Override
    protected Integer compute() {

        if (numbers.size() <= 2) {

            switch (numbers.size()) {
                case 0:
                    return 0;
                case 1:
                    return SlowProcessesUtils.sumSlow(0, numbers.get(0));
                case 2:
                    return SlowProcessesUtils.sumSlow(numbers.get(0), numbers.get(1));
            }
        }

        Integer penValue = numbers.remove(numbers.size() - 1);
        Integer lastValue = numbers.remove(numbers.size() - 2);
        SumProcessRecursiveTask sumFirst2Values = new SumProcessRecursiveTask(Arrays.asList(penValue, lastValue));
        SumProcessRecursiveTask sumTask = new SumProcessRecursiveTask(numbers);
        sumTask.fork();
        return sumFirst2Values.compute() + sumTask.join();
    }
}
