package concurrency.forkjoinexample;

import collectors.examples.collectors.examples.types.Impresora;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

public class ForkJoinMain {

    public static void main(String ... args) {

        executeNormal();
        executeWithForkJoinSplittingTask();
        executeWithForkJoinRecursive();
    }

    public static void executeNormal() {

        Impresora.imprimir("Fork Join Example", "FULL SUM NORMAL");
        LocalDateTime start = LocalDateTime.now();

        List<Integer> numbers = generateNumbersList();
        Integer result = SumProcessSequential.sumNumbers(numbers);

        System.out.println(">>>>>>>>>>> Result::" + result);
        LocalDateTime end = LocalDateTime.now();
        System.out.println("Total Time::" + start.until(end, ChronoUnit.SECONDS));
    }

    public static void executeWithForkJoinSplittingTask() {

        Impresora.imprimir("Fork Join Example", "FULL SUM WITH FORK JOIN SPLITTING");
        LocalDateTime start = LocalDateTime.now();

        List<Integer> numbers = generateNumbersList();
        Integer result = SumProcessParalell.sumNumbersSplittingBy2(numbers);

        System.out.println(">>>>>>>>>>> Result::" + result);
        LocalDateTime end = LocalDateTime.now();
        System.out.println("Total Time::" + start.until(end, ChronoUnit.SECONDS));
    }

    public static void executeWithForkJoinRecursive() {

        Impresora.imprimir("Fork Join Example", "FULL SUM WITH FORK JOIN RECURSIVE");
        LocalDateTime start = LocalDateTime.now();

        List<Integer> numbers = generateNumbersList();
        Integer result = SumProcessParalell.sumNumbersRecursive(numbers);

        System.out.println(">>>>>>>>>>> Result::" + result);
        LocalDateTime end = LocalDateTime.now();
        System.out.println("Total Time::" + start.until(end, ChronoUnit.SECONDS));
    }

    public static List<Integer> generateNumbersList() {


        return Arrays.asList(10, 20, 30, 40, 50, 60, 100, 200, 150, 60);
    }
}
