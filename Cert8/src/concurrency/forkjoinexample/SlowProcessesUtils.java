package concurrency.forkjoinexample;

public class SlowProcessesUtils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Integer sumSlow(Integer a, Integer b) {

        System.out.println("Summing " + a + " + " + b);
        sleep(1000);
        return a + b;
    }
}
