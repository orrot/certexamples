package concurrency.forkjoinexample;

import concurrency.forkjoinexample.design.SumProcessRecursiveTask;
import concurrency.forkjoinexample.design.SumProcessSplittingBy2Task;

import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class SumProcessParalell {

    public static Integer sumNumbersSplittingBy2(List<Integer> numbers) {

        ForkJoinPool pool = new ForkJoinPool();
        return pool.invoke(new SumProcessSplittingBy2Task(numbers));
    }

    public static Integer sumNumbersRecursive(List<Integer> numbers) {

        ForkJoinPool pool = new ForkJoinPool();
        return pool.invoke(new SumProcessRecursiveTask(numbers));
    }
}
