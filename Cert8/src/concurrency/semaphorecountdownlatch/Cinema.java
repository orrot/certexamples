package concurrency.semaphorecountdownlatch;

import collectors.examples.collectors.examples.types.Impresora;
import exceptions.GeneralPurposeException;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Cinema {
    
    private Set<Seat> seats;

    public Cinema(int numberOfSeats) {
        if (numberOfSeats < 1) {
            throw new GeneralPurposeException("El cinema debe tener más de 1 asiento");
        }
        this.seats = IntStream.rangeClosed(1, numberOfSeats)
                .mapToObj(number -> new Seat(number))
                .collect(Collectors.toSet());
                
    }

    public Seat viewSeat(Integer seatNumber) {
        return seats.stream()
                .filter(s -> s.getNumber().equals(seatNumber))
                .findFirst()
                .get();
    }

    public Seat selectSeat(Integer seatNumber, String personId) throws InterruptedException {
        Objects.requireNonNull(seatNumber, "Asiento no puede ser nulo");
        Seat seat = viewSeat(seatNumber);
        boolean isAccessible = seat.blockSeat();
        if (isAccessible) {
            System.out.println("   CLIENT " + personId + " HAS GOTTEN THE SEAT " + seatNumber);
            seat.setLastSelectionId(personId);
            Thread.sleep(5000);
            return seat;
        }
        System.out.println("   SEAT " + seatNumber + " IS USED. COULD NOT BE GOTTEN");
        return null;
    }

    public boolean confirmSeat(Seat seat, String personId) {
        Objects.requireNonNull(seat, "Asiento no puede ser nulo");
        Objects.requireNonNull(personId, "Se debe enviar un código de persona");
        boolean success = false;
        if (personId.equals(seat.getLastSelectionId()) && !seat.isConfirmed()) {
            seat.setPersonId(personId);
            seat.setConfirmed(true);
            success = true;
        }
        seat.releaseSeat();
        return success;
    }
    
    public void cancelSeat(Seat seat, String personId) {

        if (personId.equals(seat.getLastSelectionId())) {
            seat.releaseSeat();
        }
    }
    
    public void printSeats() {
        Impresora.imprimir("ASIENTOS", seats);
    }
}
