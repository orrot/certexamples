package concurrency.semaphorecountdownlatch;

import java.util.Objects;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Seat {

    protected Semaphore semaphore;
    private Integer number;
    private String lastSelectionId;
    private String personId;
    private boolean confirmed = false;

    public Seat(Integer number) {
        this.semaphore = new Semaphore(1, true);
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getLastSelectionId() {
        return lastSelectionId;
    }

    public void setLastSelectionId(String lastSelectionId) {
        this.lastSelectionId = lastSelectionId;
    }

    public String getPersonId()  {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
    
    public boolean blockSeat() throws InterruptedException {
        return semaphore.tryAcquire(3, TimeUnit.SECONDS);
    }

    public void releaseSeat() {
        semaphore.release();
    }
    
    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return Objects.equals(number, seat.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Seat{");
        sb.append("number=").append(number);
        sb.append(", personId='").append(personId).append('\'');
        sb.append(", confirmed=").append(confirmed);
        sb.append('}');
        return sb.toString();
    }
}
