package concurrency.semaphorecountdownlatch;

import collectors.examples.collectors.examples.types.Impresora;

import java.util.concurrent.CountDownLatch;

public class MainCinema {
    
    public static void main(String ...args) throws InterruptedException {

        CountDownLatch counter = new CountDownLatch(3);
        
        Cinema cinema = new Cinema(5);
        Impresora.imprimir("CINEMA");
        cinema.printSeats();
        System.out.println("\n\n\n");


        // Three clients wants the seat 2
        // A wants the Seat and it will try to confirm it
        // B wants the Seat, however, it will cancel the operation
        // C wants the Seat and it will try to confirm it
        
        
        Thread clienteA = new Thread(() -> {
            selectingSeatByClient(cinema, "A", 2);
            counter.countDown();
        });
        Thread clienteB = new Thread(() -> {
            regrettingSeatSelectionByClient(cinema, "B", 2);
            counter.countDown();
        });
        Thread clienteC = new Thread(() -> {
            selectingSeatByClient(cinema, "C", 2);
            counter.countDown();
        });
        
        clienteA.start();
        clienteB.start();
        clienteC.start();
        
        counter.await();
        cinema.printSeats();

    }
    
    public static void selectingSeatByClient(Cinema cinema, String clientId, Integer seatClientWants) {

        try {
            System.out.println("CLIENT " + clientId + " WANTS THE SEAT " + seatClientWants);
            Seat seat = cinema.selectSeat(seatClientWants, clientId);
            if (seat != null) {
                if (cinema.confirmSeat(seat, clientId)) {
                    System.out.println("CLIENT " + clientId + " HAS CONFIRMED THE SEAT " + seat.getNumber());
                } else {
                    System.out.println("CLIENT " + clientId + " COULD NOT CONFIRM THE SEAT " + seat.getNumber() + ". IT IS BUSY!!");
                }
                
            } else {
                System.out.println("CLIENT " + clientId + " COULD NOT GET THE SEAT " + seatClientWants);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void regrettingSeatSelectionByClient(Cinema cinema, String clientId, Integer seatClientWants) {

        try {
            System.out.println("CLIENT " + clientId + " WANTS THE SEAT " + seatClientWants);
            Seat seat = cinema.selectSeat(seatClientWants, clientId);
            if (seat != null) {
                cinema.cancelSeat(seat, clientId);
                System.out.println("CLIENT " + clientId + " HAS CANCELED THE SEAT " + seat.getNumber());
            } else {
                System.out.println("CLIENT " + clientId + " COULD NOT GET THE SEAT " + seatClientWants);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
