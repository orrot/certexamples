package concurrency.exchanger;

import collectors.examples.collectors.examples.types.Impresora;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class NoExchangerMain {
    
    public static void main(String ... args) {

        Impresora.imprimir("CONCURRENCY EXAMPLE: ", "EXCHANGERS");
        LocalDateTime start = LocalDateTime.now();

        
        List<Integer> numbers = IntStream.rangeClosed(1, 10)
                .boxed()
                .map(value -> NumberProcessing.generateNextProcessedValue())
                .collect(Collectors.toList());
        
        Integer totalProcessed = numbers.stream()
                .reduce(0, NumberProcessing::processNumbers);

        System.out.println("TOTAL PROCESSED: " + totalProcessed);
        LocalDateTime end = LocalDateTime.now();
        System.out.println("TOTAL  ELAPSED TIME: " + start.until(end, ChronoUnit.SECONDS));
    }
}
