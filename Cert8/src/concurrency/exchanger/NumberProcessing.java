package concurrency.exchanger;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Exchanger;

public class NumberProcessing {

    private CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
    private Exchanger<Integer> exchanger = new Exchanger<>();
    private Integer processedValue = 0;
    private CountDownLatch countDownLatch;
    private Integer totalProcessedValue = 0;
    
    public NumberProcessing(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }
    
    public static Integer processNumbers(Integer numA, Integer numB) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("    >> Value was processed");
        return numA + numB;
    }

    public static Integer generateNextProcessedValue() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Integer value = new Random().nextInt(10) + 1;
        System.out.println("    >> Value was generated " + value);
        return 4;
    }

    public Integer getTotalProcessedValue() {
        return totalProcessedValue;
    }

    class Generator implements Runnable {
        
        @Override
        public void run() {
            try {
                for (int i = 1; i <= 10; i++) {
                    Integer value = generateNextProcessedValue();
                    cyclicBarrier.await();
                    exchanger.exchange(value);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
            countDownLatch.countDown();
        }
    }
    
    class Processor implements Runnable {

        @Override
        public void run() {
            for (int i = 1; i <= 10; i++) {
                try {
                    cyclicBarrier.await();
                    Integer genValue = exchanger.exchange(processedValue);
                    processedValue = processNumbers(processedValue, genValue);
                    totalProcessedValue = processedValue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }
            countDownLatch.countDown();
        }
    }

    void start() {
        new Thread(new Generator()).start();
        new Thread(new Processor()).start();
    }
}
