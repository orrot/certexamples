package concurrency.exchanger;

import collectors.examples.collectors.examples.types.Impresora;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CountDownLatch;

public class ExchangerMain {

    public static void main(String ... args) throws InterruptedException {

        CountDownLatch countDownLatch = new CountDownLatch(2);
        
        Impresora.imprimir("CONCURRENCY EXAMPLE: ", "EXCHANGERS");
        LocalDateTime start = LocalDateTime.now();

        NumberProcessing numberProcessing = new NumberProcessing(countDownLatch);
        numberProcessing.start();
        
        countDownLatch.await();
        System.out.println("TOTAL PROCESSED: " + numberProcessing.getTotalProcessedValue());
        
        LocalDateTime end = LocalDateTime.now();
        System.out.println("TOTAL  ELAPSED TIME: " + start.until(end, ChronoUnit.SECONDS));
    }
}
