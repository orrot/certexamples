package concurrency.locks;

import collectors.examples.collectors.examples.types.Impresora;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MainReadWriteLock {
    
    private static ReentrantReadWriteLock lockRW = new ReentrantReadWriteLock(); 
    private static StringBuilder builder = new StringBuilder("Initial");
    
    
    public static void main(String ... args) throws InterruptedException {
        Impresora.imprimir("EXAMPLE: READ WRITE LOCKS!");
        Thread t1 = new Thread(() -> {
            while (true) {
                currentMessage();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        Thread t5 = new Thread(() -> {
            while (true) {
                currentMessage();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t5.start();
        
        Thread.sleep(5000);
        Thread t2 = new Thread(() -> appendMessage(" MESSAGE"));
        t2.start();

        Thread.sleep(2000);
        Thread t4 = new Thread(() -> appendMessage(" another"));
        t4.start();

        
    }
    
    public static void currentMessage() {
        ReentrantReadWriteLock.ReadLock lockR = lockRW.readLock();
        lockR.lock();
        try {
            System.out.println(Thread.currentThread().getName() + ": " + builder.toString());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } finally {
            lockR.unlock();
        }
    }

    public static void appendMessage(String add) {

        ReentrantReadWriteLock.WriteLock lockW = lockRW.writeLock();
        lockW.lock();
        try {
            for (char character: add.toCharArray()) {
                builder.append(character);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            lockW.unlock();
        }
    }
}
