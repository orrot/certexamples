package concurrency.locks;

import collectors.examples.collectors.examples.types.Impresora;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class MainLock {

    private static ReentrantLock lock = new ReentrantLock();

    public static Set<Integer> numbers = new HashSet<>();
    
    public static Integer generateDifferentValues() throws InterruptedException {

        try {
            if (lock.tryLock(2, TimeUnit.SECONDS)) {
                Integer newNumber = new Random().nextInt(10) + 1;
                if (numbers.contains(newNumber)) {

                    if (numbers.size() > 9) {
                        numbers.clear();
                    }
                    return generateDifferentValues();
                }
                Thread.sleep(1000);
                numbers.add(newNumber);
                return newNumber;
            } else {
                return -1;
            }
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }    
    
    public static void main(String ... args) throws InterruptedException {

        Impresora.imprimir("EXAMPLE: LOCKS!");

        Thread t1 = new Thread(() -> printNumber("VALUE 1"));
        t1.start();

        Thread t2 = new Thread(() -> printNumber("VALUE 2"));
        t2.start();

        Thread t3 = new Thread(() -> printNumber("VALUE 3"));
        t3.start();

        Thread t4 = new Thread(() -> printNumber("VALUE 4"));
        t4.start();

        Thread t5 = new Thread(() -> printNumber("VALUE 5"));
        t5.start();

        Thread t6 = new Thread(() -> printNumber("VALUE 6"));
        t6.start();

        Thread t7 = new Thread(() -> printNumber("VALUE 7"));
        t7.start();

        Thread t8 = new Thread(() -> printNumber("VALUE 8"));
        t8.start();

        Thread t9 = new Thread(() -> printNumber("VALUE 9"));
        t9.start();

        Thread t10 = new Thread(() -> printNumber("VALUE 10"));
        t10.start();
    }

    private static void printNumber(String s) {
        
        try {
            Integer value1 = generateDifferentValues();
            System.out.println(s + ": " + value1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }
}
