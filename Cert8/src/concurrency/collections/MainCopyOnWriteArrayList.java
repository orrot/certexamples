package concurrency.collections;

import collectors.examples.collectors.examples.types.Impresora;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MainCopyOnWriteArrayList {
    
    public static void main(String ... args) {

        List<String> copyOnWriteArrayList = new CopyOnWriteArrayList<>(new String[] { "A", "B"});
        runList(copyOnWriteArrayList);
        Impresora.imprimir("Copy On Write Array List", copyOnWriteArrayList);

        List<String> normalArrayList = new ArrayList<>();
        normalArrayList.add("A");
        normalArrayList.add("B");

        runList(normalArrayList);
        Impresora.imprimir("Array List", normalArrayList);
    }
    
    public static void runList(List<String> list) {

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            
            String letter = iterator.next();
            list.add("OTHER");
            list.remove("B");
            System.out.println("LETTER:" + letter);
        }
    }
}
