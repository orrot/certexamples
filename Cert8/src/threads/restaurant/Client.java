package threads.restaurant;

public class Client implements Runnable {

    public void askForMenu() throws InterruptedException {
        synchronized (RestaurantActions.menu_offered) {
            synchronized (RestaurantActions.menu_requested) {
                System.out.println("Client: Arrive the place and ask for menu ...");
                RestaurantActions.status();
                RestaurantActions.menu_requested.notify();
                RestaurantActions.menu_offered.wait();
            }
        }
    }

    public void requestForFood() throws InterruptedException {
        
        synchronized (RestaurantActions.food_requested) {
            System.out.println("Client: Request for food ...");
            RestaurantActions.status();
            RestaurantActions.food_requested.notify();
        }
    }

    public void eat() throws InterruptedException {
        
            System.out.println("Client: Eat food ...");
            RestaurantActions.status();
        
    }

    public void requestForBill() {
        System.out.println("Client: Request for Bill ...");
        RestaurantActions.status();
    }

    public void pay() {
        System.out.println("Client: Pay ...");
        RestaurantActions.status();
    }

    @Override
    public void run() {

        try {
            Thread.sleep(1500);
            askForMenu();
            requestForFood();
            eat();
            requestForBill();
            pay();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}