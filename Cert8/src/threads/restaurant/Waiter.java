package threads.restaurant;

public class Waiter implements Runnable {

    public void offerMenu() throws InterruptedException {
        synchronized (RestaurantActions.food_requested) {
            synchronized (RestaurantActions.menu_requested) {
                synchronized (RestaurantActions.menu_offered) {
                    RestaurantActions.menu_requested.wait();
                    System.out.println("Waiter: Offer Menu ...");
                    RestaurantActions.status();
                    Thread.sleep(1000);
                    RestaurantActions.menu_offered.notify();
                    RestaurantActions.food_requested.wait();
                }
            }
        }
    }

    public void serve() throws InterruptedException {
       
                System.out.println("Waiter: Serve ...");
                RestaurantActions.status();
    }

    public void generateBill() {
           System.out.println("Waiter: Generate bill ...");
            RestaurantActions.status();
                
    }

    public void receiveMoney() {
        
            System.out.println("Waiter: Pay received  ...");
            RestaurantActions.status();
        
    }

    public void checkout() {
        System.out.println("Waiter: Bye bye client  ...");
        RestaurantActions.status();
    }

    @Override
    public void run() {

        try {
            Thread.sleep(1000);
            offerMenu();
            serve();
            generateBill();
            receiveMoney();
            checkout();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
