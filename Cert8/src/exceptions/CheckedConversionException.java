package exceptions;

public class CheckedConversionException extends Exception {
    
    private String message; 
    
    public CheckedConversionException(String message) {
        
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
