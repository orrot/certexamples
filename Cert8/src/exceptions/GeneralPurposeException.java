package exceptions;

public class GeneralPurposeException extends RuntimeException {

    public GeneralPurposeException(String message) {
        super(message);
    }

    public GeneralPurposeException(String message, Throwable cause) {
        super(message, cause);
    }
}
