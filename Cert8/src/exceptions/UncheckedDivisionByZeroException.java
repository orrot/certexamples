package exceptions;

public class UncheckedDivisionByZeroException extends RuntimeException {

    private String message;

    public UncheckedDivisionByZeroException(String message) {

        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
