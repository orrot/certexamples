package inheritance;

public interface Animal {
    
    String getNombre();

    /**
     * Métodos estáticos no pueden ser sobreescritos!. Se busca evitar que una mala 
     * implementación dañe la interfaz
     */
    static void comer() {
        System.out.println("Todos los animales comen igual!");
    }
    
    
    default void aparear() {
        System.out.println("Apareamiento por defecto!");
    }
}
