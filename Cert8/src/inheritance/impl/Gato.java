package inheritance.impl;

import inheritance.Animal;
import inheritance.Terrestre;

public class Gato implements Animal, Terrestre {

    @Override
    public String getNombre() {
        return "== GATO ==";
    }

    @Override
    public void caminar() {
        System.out.println("Caminando sigiloso como los gatos!!!");
    }

    public void aparear() {
        System.out.println("Como buen felino, el apareamiento es distinto =D !");
    }
}
