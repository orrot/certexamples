package inheritance.impl;

import inheritance.Aereo;
import inheritance.Animal;

public class Ave implements Aereo, Animal {

    @Override
    public void volar() {
        System.out.println("Volando!!!");
    }

    @Override
    public String getNombre() {
        return "== AVE ==";
    }
}
