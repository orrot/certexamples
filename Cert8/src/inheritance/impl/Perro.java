package inheritance.impl;

import inheritance.Animal;
import inheritance.Terrestre;

public class Perro implements Animal, Terrestre{
    @Override
    public String getNombre() {
        return "== PERRITO ==";
    }

    @Override
    public void caminar() {
        System.out.println("Caminando a lo loco como perro!");
    }
}
