package mains;

import exceptions.CheckedConversionException;
import exceptions.UncheckedDivisionByZeroException;

public class CheckedUnchekedExceptions {

    public static boolean isNumeric(String str) {
        
        return str.matches("-?\\d+(\\.\\d+)?");
    }


    public static Integer divide(String a, String b) throws CheckedConversionException {
        
        if (!isNumeric(a) || !isNumeric(b)) {
            
            throw new CheckedConversionException("Both args should be numbers!");
        }
        
        if (Integer.valueOf(b) == 0) {
            throw new UncheckedDivisionByZeroException("Cannot divide by zero!");
        }
        return Integer.valueOf(a) + Integer.valueOf(b);
    }
    
    public static void main(String[] args) {
        System.out.println("======= Sum Example ======");
        try {
            System.out.println("14 / 15 = " + divide("14", "15"));
        } catch (CheckedConversionException ex) {
            System.out.println("CheckedConversionException launched!");
        }
        
        try {
            System.out.println("14re / 15 = " + divide("14re", "15"));
        } catch (CheckedConversionException ex) {
            System.out.println("CheckedConversionException launched!");
        }

        try {
            System.out.println("14 / 0 = " + divide("14", "0"));
        } catch (CheckedConversionException ex) {
            System.out.println("CheckedConversionException launched!");
        } catch (UncheckedDivisionByZeroException ex) {
            System.out.println("UncheckedDivisionByZeroException launched!");
        }
    }
}
