package mains;

import util.MyFileVisitor;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.AttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;

public class NIO2Examples {

    public static void main(String[] args) throws IOException {

        basicPathsExample();

        visitorExample();

        System.out.println("\n\n\n\n EXAMPLE 3\n\n");
        Path certExamples = Paths.get("/Users/orlandorodriguez/CertExamples");

        Files.walk(certExamples, 2, FileVisitOption.FOLLOW_LINKS)
            .forEach(path -> System.out.println(path.getFileName()));
    }

    private static void visitorExample() throws IOException {
        System.out.println("\n\n\n\n EXAMPLE 2\n\n");
        Path certExamples = Paths.get("/Users/orlandorodriguez/CertExamples");
        System.out.println("certExamples.getFileName()::" + certExamples.getFileName());

        Path yumbo = certExamples.resolve("Yumbo");
        System.out.println("yumbo.toAbsolutePath()::" + yumbo.toAbsolutePath());
        System.out.println("Exists?::" + Files.exists(yumbo));
        System.out.println("isDirectory?::" + Files.isDirectory(yumbo));

        System.out.println("\n My File Visitor exmaple ===== \n\n");
        Files.walkFileTree(certExamples, new MyFileVisitor());
    }

    private static void basicPathsExample() throws IOException {
        System.out.println("\n EXAMPLE 1\n\n");
        Path holaMundoFile = Paths.get("/Users/orlandorodriguez/CertExamples/External Resources/hola_mundo.txt");

        System.out.println("holaMundoFile.getFileName()::" + holaMundoFile.getFileName());
        System.out.println("holaMundoFile.getRoot()::" + holaMundoFile.getRoot());
        System.out.println("holaMundoFile.getParent()::" + holaMundoFile.getParent());
        System.out.println("holaMundoFile.toAbsolutePath()::" + holaMundoFile.toAbsolutePath());
        System.out.println("Exists?::" + Files.exists(holaMundoFile));
        System.out.println("isDirectory?::" + Files.isDirectory(holaMundoFile));
        System.out.println("size::" + Files.getAttribute(holaMundoFile, "size", LinkOption.NOFOLLOW_LINKS));

        BasicFileAttributes attributes = Files.readAttributes(holaMundoFile, BasicFileAttributes.class, LinkOption.NOFOLLOW_LINKS);
        System.out.println("size with readAttributes::" + attributes.size());

        System.out.println("\n");
        for (Path file : holaMundoFile) {
            System.out.println("Element::" + file.getFileName());
        }

        System.out.println("###########################");
        Files.readAllLines(holaMundoFile)
                .stream()
                .forEach(System.out::println);

        System.out.println("###########################");
        Files.lines(holaMundoFile)
                .forEach(System.out::println);
    }
}
