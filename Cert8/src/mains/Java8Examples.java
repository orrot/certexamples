package mains;

import java.util.Arrays;

public class Java8Examples {
    
    public static void main(String ...args) {
        
        // Peek function example
        peekExample();
    }

    private static void peekExample() {
        
        Arrays.asList(1,2,3,1,0,1)
                .stream()
                .peek(System.out::print)
                .allMatch((number) -> number < 2);
        
        
        
        
    }
}
