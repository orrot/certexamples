package mains;

import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

public class FileIOExamples {

    public static void main(String[] args) throws IOException {
        
        // Example using System.console(). It just works out of the IDE
        //consoleExample();
        
        // Example reading hello word file
        readFileBasic();
    }

    private static void readFileBasic() throws IOException {
        FileReader helloWorldFile = new FileReader("resources/hello_world.txt");

        int character = 0;
        System.out.println("=== Reading Hello World File ===");
        while ((character = helloWorldFile.read()) != -1) {

            System.out.print((char) character);
        }
        helloWorldFile.close();
    }

    private static void consoleExample() {
        System.out.print("Type username: ");
        String username = System.console().readLine();
        System.out.print("Type password: ");
        char[] password = System.console().readPassword();
        System.out.println("Username: " + username);
        String passwordString = new String(Optional.ofNullable(password).orElse(new char[1]));
        System.out.println("Password: " + passwordString);

        if ("admin".equals(passwordString)) {
            System.out.println("ADMIN ACCESSED");
        } else {
            System.out.println("NORMAL USER ACCESSED");
        }
    }
}
