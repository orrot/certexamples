package mains;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleExamples {

    public static void main(String[] args) {
        
        System.out.println("== DEFAULT LOCALTE ==");
        Locale.setDefault(Locale.US);
        System.out.println(Locale.getDefault());
        
        ResourceBundle messages = ResourceBundle.getBundle("i18n/messages");
        System.out.println("================================================");
        System.out.println(messages.getString("hello_world"));
        System.out.println(messages.getString("goodbye"));
        System.out.println(messages.getString("greeting"));

        System.out.println("================================================");

        Enumeration<String> keysIt = messages.getKeys();
        System.out.println("\n\n\n");
        while (keysIt.hasMoreElements()) {
            System.out.println("Key::" + keysIt.nextElement());
        }
        
        LocalDate today = LocalDate.now();
        LocalDateTime now = LocalDateTime.now();

        System.out.println("\n");
        System.out.println("TODAY::" + DateTimeFormatter.ISO_DATE.format(today));
        System.out.println("NOW::" + DateTimeFormatter.ISO_DATE_TIME.format(now));

        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        DateTimeFormatter dateCustom = dateTimeFormatterBuilder
                .appendLiteral("El Dia es: ")
                .appendValue(ChronoField.DAY_OF_MONTH)
                .appendLiteral(" y el mes es: ")
                .appendValue(ChronoField.MONTH_OF_YEAR)
                .toFormatter();
                
        System.out.println("TODAY::" + dateCustom.format(today));

        DateTimeFormatter dateTimeCustom = dateTimeFormatterBuilder
                .appendLiteral(" y la hora es: ")
                .appendValue(ChronoField.HOUR_OF_DAY)
                .appendLiteral(" : ")
                .appendValue(ChronoField.MINUTE_OF_HOUR)
                .toFormatter();
        
        System.out.println("NOW::" + dateTimeCustom.format(now));


        LocalDate birthday = LocalDate.of(1988, Month.AUGUST, 25);

        Period p = Period.between(birthday, today);
        long p2 = ChronoUnit.DAYS.between(birthday, today);
        System.out.println("You are " + p.getYears() + " years, " + p.getMonths() +
                " months, and " + p.getDays() +
                " days old. (" + p2 + " days total)");


    }
}
