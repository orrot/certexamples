package mains;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;

public class AnimalExample {

    public static void main(String[] args) {

        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSXXX");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String fromDateTime = String.valueOf(formatter.format(new Date()));

// toDatetime (current day + 1)-----------------------
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        String str_day= (day < 10) ? "0" + String.valueOf(day) : String.valueOf(day);
        String str_month= (month < 10) ? "0" + String.valueOf(month) : String.valueOf(month);
//log.info("str_hour -->  " + str_hour);

// toDatetime (current day + 2)-----------------------
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int day2 = calendar.get(Calendar.DAY_OF_MONTH);
        String str_day2= (day2 < 10) ? "0" + String.valueOf(day2) : String.valueOf(day2);
//log.info("str_day2 -->  " + str_day2);

// toDatetime (current day + 3)-----------------------
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        int day3 = calendar.get(Calendar.DAY_OF_MONTH);
        String str_day3= (day3 < 10) ? "0" + String.valueOf(day3) : String.valueOf(day3);
//log.info("str_day3 -->  " + str_day3);

        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        String str_year= String.valueOf(year);

//----- Assign variables 
        System.out.println("var_day " + str_day); // current day +1
        System.out.println("var_day2 " + str_day2); // current day +2
        System.out.println("var_day3 " + str_day3); // current day +3
        System.out.println("var_month " + str_month);
        System.out.println("var_year " + str_year);
        System.out.println("var_date " + fromDateTime);
        
        
    }
}
