package mains;

import threads.restaurant.Client;
import threads.restaurant.Waiter;

import java.util.concurrent.Semaphore;

public class WaitNotifyExample {
    
    public static void main(String[] args) throws InterruptedException {
        
        System.out.println("====== Threads Example ======");

        Waiter waiter = new Waiter();
        Client client = new Client();

        Thread tWaiter = new Thread(waiter);
        Thread tClient = new Thread(client);
        
        System.out.println("Thread Waiter::" + tWaiter.getName());
        System.out.println("Thread Client::" + tClient.getName());

        System.out.println("========================");
        System.out.println("========================");

        tWaiter.start();
        tClient.start();

        Semaphore sem = new Semaphore(3);
        
    }
}
