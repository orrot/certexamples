package mains;

import collectors.examples.collectors.examples.types.Impresora;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.Map;

public class DateFormatExamples {
    
    public static void main(String ... args) {

        // Legacy dates
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT);
        DateFormat dateFormatLongShort = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
        
        Impresora.imprimir(dateFormat.format(new Date()));
        Impresora.imprimir(dateFormatLongShort.format(new Date()));

        // Java 8 Dates
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendLiteral("Mi mes: ")
                .appendPattern("MMMM")
                .appendLiteral(" Mi día: ")
                .appendPattern("d")
                .toFormatter();
        
        String dateTime = formatter.format(LocalDateTime.now());
        String date = formatter.format(LocalDateTime.now());
        
        Impresora.imprimir(dateTime);
        Impresora.imprimir(date);
        
    }
    
     
}
