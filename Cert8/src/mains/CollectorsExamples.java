package mains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class CollectorsExamples {

    public static void main(String[] args) {
        
        List<String> cadenas = Arrays.asList("astucia", "aceitunas", "tambor", "bebe", "chicle", "Armonia");
        
        // grouping by example
        System.out.println("\n== palabrasPorLetra ==\n");
        Map<String, List<String>> palabrasPorLetra =
            cadenas.stream()
                .filter(Objects::nonNull)
                .filter(cadena -> !"".equals(cadena))
                .collect(Collectors.groupingBy(cadena -> 
                        String.valueOf((cadena).charAt(0)).toLowerCase()));
        
        palabrasPorLetra.forEach((key, value) -> System.out.println(key + " ==> " + value));
        
        // grouping by with special Collector
        System.out.println("\n\n== palabrasPorLetraConcat ==\n");
        Map<String, String> palabrasPorLetraConcat =
                cadenas.stream()
                        .filter(Objects::nonNull)
                        .filter(cadena -> !"".equals(cadena))
                        .collect(Collectors.groupingBy(cadena ->
                                        String.valueOf((cadena).charAt(0)).toLowerCase(),
                                Collectors.joining(" , ")));

        palabrasPorLetraConcat.forEach((key, value) -> System.out.println(key + " ==> " + value));

        
        // flatMap example
        System.out.println("\n\n== palabrasConT ==\n");
        
        palabrasPorLetra.entrySet()
                .stream()
                .flatMap(entry -> entry.getValue().stream())
                .filter(palabra -> palabra.toLowerCase().contains("t"))
                .forEach(System.out::println);

    }
}
