package mains;

import util.AutocloseableResource;

import java.util.Arrays;
import java.util.Scanner;

public class TryCatchWithResources {

    public static void main(String [] args) {
        
        // Scan example
        //scanExample();
        
        // AutocloseableExample
        autocloseableExample();
        
        // AutocloseableExample 
        autocloseableExampleWithResource();

    }

    private static void autocloseableExample() {
        
        System.out.println("======= autocloseableExample ======");
        try (AutocloseableResource autoCloseable = new AutocloseableResource()) {
            autoCloseable.open();
        } catch (Exception e) {
            System.out.println("Exception launch!");
        }
    }

    private static void autocloseableExampleWithResource() {
        
        System.out.println("======= autocloseableExampleWithResource ======");
        try (AutocloseableResource autoCloseable = new AutocloseableResource()) {
            autoCloseable.open();
            autoCloseable.launchInvalidOperation();
        } catch (Exception e) {
            System.out.println("Exception launch!");
            Arrays.stream(e.getSuppressed())
                    .forEach(ex -> System.out.println(ex.getMessage()));
        } finally {
            System.out.println("Finally block");
        }
    }

    private static void scanExample() {
        System.out.println("Type an integer in the console: ");
        try(Scanner consoleScanner = new Scanner(System.in)) {
            
            System.out.println("You typed the integer value: " + consoleScanner.nextInt());
        } catch(Exception e) {
            // catch all other exceptions here ...
            System.out.println("Error: Encountered an exception and could not read an integer from the console... ");
            System.out.println("Exiting the program - restart and try the program again!");
        }
    }
}
