package mains;

public class ThreadsMain {

    public static long count = 0;
    
    public static class Counter implements Runnable {
        
        public synchronized void increment() {
            count++;
            System.out.println("Count:: " + count + " , current Thread: " + Thread.currentThread().getName());
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            increment();
            increment();
            increment();
            increment();
            increment();
            increment();
            increment();
            increment();
        }
    }

    public static void main(String[] args) throws InterruptedException {

        System.out.println("====== Threads Example ======");
        
        Counter counter = new Counter();
        Thread t1 = new Thread(counter);
        Thread t2 = new Thread(counter);
        Thread t3 = new Thread(counter);
        
        System.out.println("Thread 1::" + t1.getName());
        System.out.println("Thread 2::" + t2.getName());
        System.out.println("Thread 3::" + t3.getName());
        t1.start();
        t2.start();
        t3.start();
        
        t1.join();
        t2.join();
        t3.join();
        System.out.println("==== END ====");
        // Without any blocking mechanism the printed numbers don't go ordered. Sometimes it can be some repeated numbers.
        // With synchronized keyword in the method the printed numbers go ordered, however the threads are not in order
        // The join is just out to ensure that the Main Thread is finished after the others
    }
}
