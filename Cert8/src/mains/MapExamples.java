package mains;

import collectors.examples.collectors.examples.types.Impresora;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;
import java.util.Map;

public class MapExamples {
    
    public static void main(String ...args) {

        Map<Integer, String> maps = new HashMap<>();
        maps.put(10, "Carlos");
        maps.put(30, "Laura");
        maps.put(60, "Manuel");
        maps.put(80, null);
        maps.put(null, "Felipe");

        Impresora.imprimir("Mi Mapa", maps);
        
        String threeFirstCharsFor10 = maps.compute(10, (k,v) -> v.substring(0,3));
        Impresora.imprimir("threeFirstCharsFor10", threeFirstCharsFor10);

        String threeFirstCharsFor20 = maps.compute(20, (k,v) -> Boolean.toString(v == null));
        Impresora.imprimir("threeFirstCharsFor20", threeFirstCharsFor20);

        String ifPresentExample = maps.computeIfPresent(10, (k,v) -> v.substring(0, 3));
        Impresora.imprimir("ifPresentExample", ifPresentExample);

        String ifAbsentExample = maps.computeIfAbsent(20, k -> k.toString() + "THIS");
        Impresora.imprimir("ifAbsentExample", ifAbsentExample);

        Impresora.imprimir("Mi Mapa", maps);

    }
}
